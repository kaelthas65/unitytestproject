﻿using UnityEngine;

public class GameLoader : MonoBehaviour
{
    public GameObject tileMapManager;

    private void Awake()
    {
        Instantiate(tileMapManager);
    }
}
