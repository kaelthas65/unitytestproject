﻿using System.Collections.Generic;
using Assets.Scripts.HeightMapProviders;
using UnityEngine;
using Random = UnityEngine.Random;

public class TilemapManager : MonoBehaviour
{
    public int mapSizePower = 7;
    public float roughness = 20f;

    public List<GameObject> grassTiles;
    public List<GameObject> sandTiles;
    public List<GameObject> waterTiles;

    private const float WaterLine = 0.2f;
    private const float SandLine = 0.7f;
    private float[,] heightMap;

    private IHeightMapProvider heightMapProvider;

    private void Awake()
    {
        heightMapProvider = new DiamondSquareHeightMapProvider(mapSizePower, roughness);
        heightMap = heightMapProvider.Generate();
        GenerateTileMap();
    }

    private void GenerateTileMap()
    {
        var maxSideSize = heightMap.GetLength(1);
        for (var y = 0; y < maxSideSize; y++)
        {
            for (var x = 0; x < maxSideSize; x++)
            {
                var tile = ResolveTile(heightMap[x, y]);
                Instantiate(tile, new Vector3(x, y, 0), Quaternion.identity);
            }
        }
    }

    private GameObject ResolveTile(float height)
    {
        List<GameObject> tiles;

        if (height <= WaterLine)
        {
            tiles = waterTiles;
        }
        else if (IsSandLine(height))
        {
            tiles = sandTiles;
        }
        else
        {
            tiles = grassTiles;
        }

        return tiles[Random.Range(0, tiles.Count - 1)];
    }

    private bool IsSandLine(float height)
    {
        return height > WaterLine && height <= SandLine;
    }
}
